//
//  WXCustomModule.h
//  weexDemoProject
//
//  Created by nimo on 2016/12/16.
//  Copyright © 2016年 nimo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXImgLoaderProtocol.h>

@interface WXCustomModule : NSObject<WXModuleProtocol,UIAlertViewDelegate>
+ (instancetype)sharedInstance;
@property(nonatomic,strong) NSDictionary *param;
@end
