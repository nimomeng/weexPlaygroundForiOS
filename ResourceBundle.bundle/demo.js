/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var __weex_template__ = __webpack_require__(1)
	var __weex_style__ = __webpack_require__(2)
	var __weex_script__ = __webpack_require__(3)

	__weex_define__('@weex-component/df4e14e4c3b5f40d1d5df575f8a9e9c7', [], function(__weex_require__, __weex_exports__, __weex_module__) {

	    __weex_script__(__weex_module__, __weex_exports__, __weex_require__)
	    if (__weex_exports__.__esModule && __weex_exports__.default) {
	      __weex_module__.exports = __weex_exports__.default
	    }

	    __weex_module__.exports.template = __weex_template__

	    __weex_module__.exports.style = __weex_style__

	})

	__weex_bootstrap__('@weex-component/df4e14e4c3b5f40d1d5df575f8a9e9c7',undefined,undefined)

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = {
	  "type": "div",
	  "children": [
	    {
	      "type": "list",
	      "classList": [
	        "list"
	      ],
	      "children": [
	        {
	          "type": "cell",
	          "append": "tree",
	          "classList": [
	            "cell"
	          ],
	          "events": {
	            "click": "oncellclick"
	          },
	          "repeat": function () {return this.dataList},
	          "children": [
	            {
	              "type": "div",
	              "classList": [
	                "img"
	              ],
	              "children": [
	                {
	                  "type": "image",
	                  "style": {
	                    "width": 76,
	                    "height": 76
	                  },
	                  "attr": {
	                    "resize": "contain",
	                    "src": "http://avatar.csdn.net/0/2/4/1_mylizh.jpg"
	                  }
	                }
	              ]
	            },
	            {
	              "type": "div",
	              "classList": [
	                "describe"
	              ],
	              "children": [
	                {
	                  "type": "text",
	                  "style": {
	                    "fontSize": 20,
	                    "color": "#00ff00"
	                  },
	                  "attr": {
	                    "value": function () {return 'The Sequence number is:' + (this.$index)}
	                  }
	                },
	                {
	                  "type": "text",
	                  "classList": [
	                    "bottomText"
	                  ],
	                  "attr": {
	                    "value": function () {return 'The random number is ' + (this.randomValue)}
	                  }
	                }
	              ]
	            }
	          ]
	        }
	      ]
	    }
	  ]
	}

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = {
	  "cell": {
	    "height": 100,
	    "borderColor": "#cccccc",
	    "borderWidth": 1,
	    "flexDirection": "row",
	    "alignItems": "center"
	  },
	  "describe": {
	    "marginLeft": 20,
	    "flexDirection": "column"
	  },
	  "bottomText": {
	    "fontSize": 10,
	    "color": "#b1b1b1",
	    "width": 500
	  }
	}

/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = function(module, exports, __weex_require__){'use strict';

	module.exports = {
		data: function () {return {
			dataList: []
		}},
		methods: {
			oncellclick: function oncellclick(e) {
				var customModel = __weex_require__('@weex-module/testModule');
				customModel.setParamToNative(e.target.attr.rownumber + " was clicked and this is param from Weex");
				var that = this;
				customModel.callback(function (res) {
					that.$call('modal', 'toast', {
						'message': e.target.attr.rownumber + ' clicked and Here the message from Native' + res.result,
						'duration': 2.0
					});
				});
			}
		},
		ready: function ready(e) {

			var params = this.$getConfig();
			this.$call('modal', 'toast', {
				'message': ' Data from Native is : ' + params.test,
				'duration': 2.0
			});
			var that = this;
			var customModel = __weex_require__('@weex-module/testModule');
			customModel.callback(function (res) {
				var listLengh = res.listLength;
				var dataList = getData(listLengh);
				that.dataList = dataList;
			});
		}
	};
	function getData(length) {
		var array = [];
		for (var i = 0; i < length; i++) {
			var obj = {};
			obj.id = i;
			obj.name = "name " + i;
			obj.randomValue = '' + parseInt(Math.random() * 100000 / 100);
			array.push(obj);
		}
		return array;
	}}
	/* generated by weex-loader */


/***/ }
/******/ ]);