//
//  ViewController.m
//  weexDemoProject
//
//  Created by nimo on 2016/12/7.
//  Copyright © 2016年 nimo. All rights reserved.
//

#import "ViewController.h"
#import <WeexSDK/WXSDKInstance.h>
#import "WXCustomModule.h"

@interface ViewController ()
@property(nonatomic, strong) WXSDKInstance *instance;
@property(nonatomic, strong) UIView *weexView;
@property(nonatomic, assign) CGFloat weexHeight;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _weexHeight =CGRectGetHeight(self.view.frame) - 64;
    [self.navigationController.navigationBar setHidden:YES];
    [self render];
}

- (void)dealloc
{
    [_instance destroyInstance];
}

- (void)render {
    if (!self.instance) {
        self.instance = [[WXSDKInstance alloc] init];
    }
    self.instance.viewController = self;
    CGFloat width = CGRectGetWidth(self.view.frame);
    self.instance.frame = CGRectMake(0, 20, width, self.weexHeight);
    
    __weak typeof (self) wSelf = self;
    self.instance.onCreate = ^ (UIView *view)
    {
        [wSelf.weexView removeFromSuperview];
        wSelf.weexView = view;
        [wSelf.view addSubview:wSelf.weexView];
        UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, wSelf.weexView);
    };
    _instance.renderFinish = ^(UIView *view) {
        NSLog(@"render finished");
    };
    NSString *url = [NSString stringWithFormat:@"file://%@/ResourceBundle.bundle/demo.js",[NSBundle mainBundle].bundlePath];
//    first way to pass param
    [_instance renderWithURL:[NSURL URLWithString:url] options:@{@"bundleUrl":url,@"test":@"helloTest"} data:nil];
    WXCustomModule *customModule = [WXCustomModule sharedInstance];
//    second way to pass param
    customModule.param = @{@"listLength":@(7)};
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
