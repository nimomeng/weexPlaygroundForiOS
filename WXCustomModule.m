//
//  WXCustomModule.m
//  weexDemoProject
//
//  Created by nimo on 2016/12/16.
//  Copyright © 2016年 nimo. All rights reserved.
//

#import "WXCustomModule.h"

@implementation WXCustomModule
@synthesize weexInstance;

+ (instancetype)sharedInstance
{
    static WXCustomModule *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[WXCustomModule alloc] init];
    });
    return instance;
}

WX_EXPORT_METHOD(@selector(callback:))
WX_EXPORT_METHOD(@selector(setParamToNative:))
- (void)callback:(WXModuleCallback)callback
{
    callback([WXCustomModule sharedInstance].param);
}

- (void)setParamToNative:(NSString *)param
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"AlertView" message:param delegate:self cancelButtonTitle:@"NO" otherButtonTitles:nil, nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"");
}
@end
