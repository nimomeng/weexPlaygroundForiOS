//
//  WXImgLoaderDefaultImpl.h
//  weexDemoProject
//
//  Created by nimo on 2016/12/8.
//  Copyright © 2016年 nimo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXImgLoaderProtocol.h>

@interface WXImgLoaderDefaultImpl : NSObject<WXImgLoaderProtocol, WXModuleProtocol>


@end
